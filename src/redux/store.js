import { createStore } from 'redux';


const initialState = {
	todoList: [],
	inProgressList: [],
	completeList: []
}

function reducer(state = initialState, action) {
	// console.log('reducer', state, action);

	switch(action.type) {
    case 'UPDATE':
			let currentState = JSON.parse(JSON.stringify(state));
			currentState[action.payload.table] = action.payload.data;
			return currentState;
    default:
      return state;
  }
}
  
const Store = createStore(reducer);
// store.dispatch({ type: "INCREMENT" });
// store.dispatch({ type: "INCREMENT" });
// store.dispatch({ type: "DECREMENT" });
// store.dispatch({ type: "RESET" });

export default Store;