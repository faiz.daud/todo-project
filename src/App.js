import React from 'react';
import logo from './logo.svg';
import './App.css';

import AddTodo from './add-todo/add-todo';
import TaskCounter from './share/task-counter/task-counter';
import TaskList from './share/task-list/task-list';

import { connect } from 'react-redux';
import ApiService from './api/api';
import Test from './share/test/test';

class App extends React.Component {
	// state = {
	// 	todoList: [
	// 		{id: 1, content: 'aaa'}, //-->{id: 0, text: '', status: 0, order: 0}
	// 		{id: 2, content: 'bbb'}
	// 	],
	// 	inProgressList: [],
	// 	completeList: []
	// }
	componentWillMount() {
		this.loadList();
		// this.loadStatusMap();
	}

	loadList() {
		ApiService.createAPI().get('/cards').then(res => {
			if(res.ok) {
				let list = res.data;
				console.log(list);
				let statusZero = list.filter(item => {
					return item.status === 0;
				});
				let statusOne = list.filter(item => {
					return item.status === 1;
				})
				let statusTwo = list.filter(item => {
					return item.status === 2;
				})
				// console.log(list);
				this.props.dispatch({type: 'UPDATE', payload: {table: 'todoList', data: statusZero}});
				this.props.dispatch({type: 'UPDATE', payload: {table: 'inProgressList', data: statusOne}});
				this.props.dispatch({type: 'UPDATE', payload: {table: 'completeList', data: statusTwo}});
			}
			else {
				console.log(res);
				// error handler
			}
		});
	}

	loadStatusMap() {
		ApiService.createAPI().get('/MAP_STATUS').then(res => {
			if(res.ok) {
				console.log(res.data);
			}
			else {
				console.log(res);
				// error handler
			}
		});
	}

	deleteTodo = (id) => {
		let todo = this.props.todoList.filter(todo => {
			return todo.id !== id;
		});

		// this.setState({todos});
		this.props.dispatch({type: 'UPDATE', payload: {table: 'todoList', data: todo}});
	}

	addTodo = (todo) => {
		todo.id = Math.random();
		todo.status = 0;
		todo.order = this.props.todoList.length;

		let mTodo = [...this.props.todoList, todo];

		// this.setState({todos});
		this.props.dispatch({type: 'UPDATE', payload: {table: 'todoList', data: mTodo}});
	}

	// addTask = (ev, title) => {
	// 	let item = JSON.parse(ev.dataTransfer.getData('text'));

	// 	if(title === item.from) return;
	// 	else if(title === 'Todo') {
	// 		this.setState({
	// 			todos: [...this.state.todos, item],
	// 			inProgress: this.state.inProgress.filter(task => {
	// 				return task.id !== item.id;
	// 			}),
	// 			complete: this.state.complete.filter(task => {
	// 				return task.id !== item.id;
	// 			})
	// 		});
	// 	}
	// 	else if(title === 'In Progress') {
	// 		this.setState({
	// 			inProgress: [...this.state.inProgress, item],
	// 			todos: this.state.todos.filter(task => {
	// 				return task.id !== item.id;
	// 			}),
	// 			complete: this.state.complete.filter(task => {
	// 				return task.id !== item.id;
	// 			})
	// 		});
	// 	}
	// 	else if(title === 'Done') {
	// 		this.setState({
	// 			complete: [...this.state.complete, item],
	// 			todos: this.state.todos.filter(task => {
	// 				return task.id !== item.id;
	// 			}),
	// 			inProgress: this.state.inProgress.filter(task => {
	// 				return task.id !== item.id;
	// 			})
	// 		});
	// 	}
	// }

	getOverallCount = () => {
		let todoCount = this.props.todoList.length;
		let inProgressCount = this.props.inProgressList.length;
		let completeCount = this.props.completeList.length;

		let totalCount = todoCount + inProgressCount + completeCount;

		return totalCount;
	}

	render() {
		return (
			<div style={{textAlign: 'center', padding: 10}}>
				<div style={{textAlign: 'left'}}>
					<AddTodo addTodo={this.addTodo}/>
				</div>
				<div style={{display: 'flex', justifyContent: 'flex-end', marginRight: 30, marginBottom: 10}}>
					<TaskCounter count={this.getOverallCount()}/>
				</div>
				<div style={{display: 'flex', flex: 1, flexWrap: 'wrap', flexDirection: 'row'}}>
					<TaskList 
						title='Todo' 
						list={this.props.todoList}
						// addTask={this.addTask}
						deleteTask={this.deleteTodo}/>
					<TaskList 
						title='In Progress' 
						list={this.props.inProgressList}
						// addTask={this.addTask}
						deleteTask={this.deleteTodo}/>
					<TaskList
						title='Done' 
						list={this.props.completeList}
						// addTask={this.addTask}
						deleteTask={this.deleteTodo}/>
				</div>
				<Test/>
			</div>
		);
	}
}

function mapStateToProps(state) {
  return {
		todoList: state.todoList,
		inProgressList: state.inProgressList,
		completeList: state.completeList
  };
}

// export default App;
export default connect(mapStateToProps)(App);
