import { create } from 'apisauce';
import { Config } from './config';


export default class ApiService {
  static createAPI() {
    const api = create({
      baseURL: `${Config.API_URL}`,
      timeout: Config.API_TIMEOUT,
      headers: {
        'Content-Type': 'application/json',
      }
    });

    return api;
  }
}