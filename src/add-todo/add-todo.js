import React from 'react';

class AddTodo extends React.Component {
	state = {
		text: ''
	}

	handleChange = (e) => {
		this.setState({
			text: e.target.value
		});
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.props.addTodo(this.state);
		this.setState({text: ''});
	}

	render() {
		return(
			<div style={{margin: 5}}>
				<form onSubmit={this.handleSubmit}>
					<label style={{fontSize: 20, fontWeight: 'bold', marginRight: 5}}>Add Task</label>
					<input 
						style={{height: 30, width: 200, borderStyle: 'solid', borderRadius: 3, borderColor: 'transparent', fontSize: 20}} 
						type='text' onChange={this.handleChange} value={this.state.text}/>
				</form>
			</div>
		);
	}
}

export default AddTodo;