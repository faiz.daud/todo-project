import React from 'react';
import { connect } from 'react-redux';

import './task-list.css';
import TaskCounter from '../task-counter/task-counter';


// const TaskList = ({title, list, deleteTask, addTask}) => {
const TaskList = (props) => {
	let title = props.title;
	let list = props.list;
	// let deleteTask = props.deleteTask;
	let deleteTask = () => {};

	const taskList = list.length ? (
		list.map(item => {
			return(
				<div key={item.id}>
				<div 
					// key={item.id}
					// onMouseOver=''
					className='grabbable'
					style={{display: 'flex', height: 50, backgroundColor: 'white'}}
					onDragOver={(e) => onDragOver(e)}
					onDrop={(e) => onDrop(e, item)}
					onDragStart={(e) => {
						item.from = title;
						onDragStart(e, item);
					}}
					draggable>
					<span 
						style={{display: 'flex', alignItems: 'center', padding: 10, textAlign: 'left'}} 
						onClick={() => deleteTask(item.id)}>{item.text}</span>
				</div>
				<div style={{height: 2, width: '100%'}}/>
				</div>
			);
		})
	) : (
		<div
			className='grabbable'
			style={{display: 'flex', height: 50}}
			onDragOver={(e) => onDragOver(e)}
			onDrop={(e) => onDrop(e, null)}>
			<p style={{display: 'flex', alignItems: 'center', padding: 10, textAlign: 'left'}}>You have no tasks left.</p>
		</div>
	);

	const onDragStart = (ev, item) => {
		ev.persist();
		// console.log('onDragStartEv: ', ev);
		// console.log('onDragStartItem: ', item);
		ev.dataTransfer.setData('text/plain', JSON.stringify(item));
	}

	const onDragOver = (ev) => {
		ev.preventDefault();
	}

	function compare( a, b ) {
		if ( a.order < b.order ){
			return -1;
		}
		if ( a.order > b.order ){
			return 1;
		}
		return 0;
	}

	function getTableName(title) {
		if(title === 'Todo') return 'todoList';
		else if(title === 'In Progress') return 'inProgressList';
		else return 'completeList';
	}

	function getList(title) {
		if(title === 'Todo') return JSON.parse(JSON.stringify(props.todoList));
		else if(title === 'In Progress') return JSON.parse(JSON.stringify(props.inProgressList));
		else return JSON.parse(JSON.stringify(props.completeList));
	}

	const addTask = (ev, title, targetItem) => {
		let item = JSON.parse(ev.dataTransfer.getData('text'));
		if(title !== item.from) removeTask(item);
		if(targetItem === null) {
			item.order = 0;
			props.dispatch({type: 'UPDATE', payload: {table: getTableName(title), data: [item]}});
			return;
		}

		let listFiltered = getList(title).filter(task => {
			return task.id !== item.id;
		});
		for(let i=0; i<listFiltered.length; i++) {
			listFiltered[i].order = i;
		}
		
		listFiltered.splice(targetItem.order, 0, item);
		for(let i=0; i<listFiltered.length; i++) {
			listFiltered[i].order = i;
		}
		listFiltered.sort(compare);

		props.dispatch({type: 'UPDATE', payload: {table: getTableName(title), data: listFiltered}});
	}

	const removeTask = (item) => {
		let listFiltered = getList(item.from).filter(task => {
			return task.id !== item.id;
		});
		for(let i=0; i<listFiltered.length; i++) {
			listFiltered[i].order = i;
		}

		listFiltered.sort(compare);

		props.dispatch({type: 'UPDATE', payload: {table: getTableName(item.from), data: listFiltered}});
		return;
	}

	const onDrop = (ev, targetItem) => {
		// ev.persist();
		ev.preventDefault();
		addTask(ev, title, targetItem);
	}

	return(
		<div
			style={{margin: 10, display: 'flex', flex: 1, flexDirection: 'column', minWidth: 300}}>

			<div 
				style={{display: 'flex', flexDirection: 'row', padding: 10, backgroundColor: 'grey', 
					justifyContent: 'center'}}>
				<div 
					style={{display: 'flex', flex: 1, fontSize: 20, fontWeight: 'bold', alignItems: 'center', color: 'white'}}>{title}</div>
				<div style={{display: 'flex'}}>
					<TaskCounter count={list.length}/>
				</div>
			</div>

			{taskList}
		</div>
	);
}

// export default TaskList;
function mapStateToProps(state) {
  return {
		todoList: state.todoList,
		inProgressList: state.inProgressList,
		completeList: state.completeList
  };
}

export default connect(mapStateToProps)(TaskList);