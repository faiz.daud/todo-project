import React from 'react';


const printFB = (integer) => {
	if(integer % 3 === 0 && integer % 5 === 0) return 'FizzBuzz';
	if(integer % 3 === 0) return 'Fizz';
	if(integer % 5 === 0) return 'Buzz';
	else return 'Input must be integer';
}

const mathTest = (str) => {
	let array = str.split('');
	let newArray = [];
	let index = str.length;
	for(let i=0; i<= str.length; i++) {
		newArray.push(array[index]);
		index--;
	}
	let reverse = newArray.join('');
	let result = str === reverse;
	return result ? 'true' : 'false';
}

const Test = () => {
  return(
		<div style={{width: '100%', height: 50, backgroundColor: 'yellow', display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
			<h3 style={{alignSelf: 'center', margin: 0, padding: 0}}>{mathTest('kayak')}</h3>
		</div>
	);
}

export default Test;