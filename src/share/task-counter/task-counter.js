import React from 'react';


const TaskCounter = ({count}) => {
  return(
		<div style={{backgroundColor: 'white', width: 100, height: 50, display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
			<h3 style={{alignSelf: 'center', margin: 0, padding: 0}}>{count}</h3>
			<h3 style={{alignSelf: 'center', margin: 0, padding: 0, color: 'grey'}}>TASKS</h3>
		</div>
	);
}

export default TaskCounter;